package com.j25.recursive;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        rekurencyjniePotega(2, 10);

//        System.out.println(rekurencyjnieOdworcString("tekst", 1));

//        System.out.println(rekurencyjnieOdworcString("piesek"));

        Map<Integer, Integer> fibo = new HashMap<Integer, Integer>();
        System.out.println(F(30, fibo));
        System.out.println(F(30));
    }

    // n! = [n] * [(n-1)] * [(n-2)] * ... * [2] * [1]
    public static int rekurencyjnieSilnia(int n) {
        if (n == 1) {
            return 1;
        }
        /*mnożę siebie razy element niżej (który otrzymałem z poprzego zagłębienia)*/
        return rekurencyjnieSilnia(n - 1) * n;
    }

    // n^m = n * n * n .... ( m krotność mnożenia przez siebie n)
    public static long rekurencyjniePotega(int n, int m) {
        if (m == 1) {
            return n;
        }
        return rekurencyjniePotega(n, m - 1) * n;
    }

    // "tekst" - > "tsket"
    // odwrócenie string'a (reverse)
    public static String rekurencyjnieOdworcString(String tekst, int i) {
        if (tekst.length() == 1) {
            return tekst;
        }
        char literkaTejIteracji = tekst.charAt(0);

        printNMarks(i);
        System.out.println("Wchodzę do rekurencji: " + tekst.substring(1));
        String wynikRekurencji = rekurencyjnieOdworcString(tekst.substring(1), ++i);
        printNMarks(i);
        System.out.println("Wychodzę z rekurencji: " + wynikRekurencji);

        wynikRekurencji = wynikRekurencji + literkaTejIteracji;
        printNMarks(i);
        System.out.println("Dodaję literkę, obecnie: " + wynikRekurencji);

        return wynikRekurencji;
    }

    private static void printNMarks(int n) {
        for (int i = 0; i < n; i++) {
            System.out.print(">");
        }
    }


    public static int F(int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        return F(n-1) + F(n-2);
    }

    public static int F(int n, Map<Integer, Integer> fibo) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        if (!fibo.containsKey(n - 1)) {
            fibo.put(n - 1, F(n - 1, fibo));
        }
        if (!fibo.containsKey(n - 2)) {
            fibo.put(n - 2, F(n - 2, fibo));
        }
        return fibo.get(n - 1) + fibo.get(n - 2);
    }

}
