package com.j25.recursive;

public class MainDziesietnaNaBinarna {
    public static void iteracyjnie(int liczba) {
        // dla obliczenia wartości binarnej pewnej liczby dziesiętnej

        // pętla
        // dzielimy liczbę przez 2 - resztę z dzielenia zapisujemy (0 lub 1)
        // wynik ponownie dzielimy przez 2 i resztę zapisujemy
        // powtarzamy proces dopóki wynik nie będzie 1
        StringBuilder builder = new StringBuilder();

        while (liczba > 1) {
            int reszta = liczba % 2;
            if (reszta == 1) {
                builder.append("1");
            } else {
                builder.append("0");
            }

            liczba = liczba / 2;

        }
        builder.append("1");

        System.out.println(builder.reverse().toString());
    }

    public static void main(String[] args) {
        System.out.println(binary(512));
    }

    public static String binary(int liczba) {
        if (liczba == 1) {
            return "1";
        }
        if (liczba == 0) {
            return "0";
        }

        String binaryLiczba = binary(liczba / 2) + String.valueOf(liczba % 2);

        return binaryLiczba;
    }

}
