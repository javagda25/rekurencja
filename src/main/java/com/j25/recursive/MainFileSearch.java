package com.j25.recursive;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainFileSearch {
    public static void main(String[] args) {
        /**
         * Stwórz aplikację która pozwala podać:
         * - katalog
         * - nazwę pliku (lub frazę szukaną w pliku)
         *
         * a program przeszuka katalog rekurencyjnie i zwróci listę plików zawierających szukaną frazę w nazwie (lub treści*).
         */

        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj katalog:");
        String katalog = scanner.nextLine();

        System.out.println("Podaj szukaną frazę:");
        String szukanaFraza = scanner.nextLine();

        File file = new File(katalog); // tworzę obiekt "deskryptora plików" - który zawiera informacje o danym pliku/katalogu

        List<File> znalezionePliki = new ArrayList<File>();

        if (file.exists() && file.isDirectory()) {
            szukajFrazy(file, szukanaFraza.toLowerCase(), znalezionePliki);
        }

        znalezionePliki.forEach(System.out::println);
    }

    private static void szukajFrazy(File file, String szukanaFraza, List<File> wynik) {
        File[] plikiWKatalogu = file.listFiles();
        if (plikiWKatalogu == null || plikiWKatalogu.length > 0) {
            for (File singleFile : plikiWKatalogu) {
                if (singleFile.isFile()) {
                    if (singleFile.getName().toLowerCase().contains(szukanaFraza)) {
                        wynik.add(singleFile);
                    }
                } else if (singleFile.isDirectory()) {
                    szukajFrazy(singleFile, szukanaFraza, wynik); // rekurencja (szukaj wgłąb)
                }
            }
        }
    }
}
