package com.j25.recursive;

public class MainSprawdzPalindrom {
    public static void main(String[] args) {
        String tekst = "abccba";

        System.out.println(czyJestPalindrom(tekst.toLowerCase()));
    }

    public static boolean czyJestPalindrom(String tekst) {
        if (tekst.length() <= 1) {
            return true;
        }

        return tekst.charAt(0) == tekst.charAt(tekst.length() - 1) && // ta linia sprawdza czy pierwsza i ostatnia literka jest taka sama
                czyJestPalindrom(tekst.substring(1, tekst.length() - 1)); // później (po porównaniu) wchodzę wgłąb i porównuję substring bez
                                                                            // pierwszego i ostatniego znaku
    }
}
